# XLSX2QR

Libreria básica para transformar el contenido de un archivo XLSX (o XLS) en QRs que almacenan en formato JSON la información

## ¿Que se supone que hace?

Toma como entrada un archivo Excel, cuya primer fila tiene los nombres de los atributos de cada columna, y el resto de las filas, entradas a transformar.

## ¿..y que se supone que genera?

La salida son imagenes QR, una por cada fila, que tienen como dato un JSON con todos los pares \{clave:valor\} de cada entrada.

## ¿¿eso es útil??

La motivación principal, fue cumplir un deadline laboral. Podemos decir que este script Python, básico y sucio, es la representación mas pura de "automate the boring stuff". Es útil si quiero seguir percibiendo un sueldo :).

## Me convenciste, ¿que necesito para usarlo?
Es simple, solo se necesita tener instalado Python 3 (en cualquiera de sus versiones). Descargar el repositorio, e instalar los requerimientos con el comando 

    pip install -r requirements.txt
o si en tu sistema co-existen Python 2 y Python 3, probablemente necesites ejecutar

    pip3 install -r requirements.txt

Este comando instala automáticamente las librerías que necesita el script para funcionar.
¡Ahora, sólo falta que haga su magia!

    python xlsx2qr.py archivo.xlsx 

Con ese simple comando ya se están generando tus códigos qr en base a lo detallado en el archivo de hojas de cálculo.

# FAQ

> Estoy usando Windows 7 o superior y el comando no se ejecuta correctamente.

Probablemente los binarios Python no estén siendo invocados de la forma adecuada. En algunos sistemas en necesario correr el siguiente comando para ejecutar el script 

    py -3 xlsx2qr.py archivo.xlsx

Siempre que ejecutamos estos comandos, se hace posicionados en la misma carpeta donde se encuentra el archivo *xlsx2qr*