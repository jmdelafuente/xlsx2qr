# -*- coding: utf-8 -*-
import argparse
import sys
from collections import OrderedDict
from pathlib import Path

import qrcode
import simplejson as json
import xlrd
import unidecode


def main(args):
   inputfile = args.archivo
   print('Se esta procesando el siguiente archivo ' + inputfile)
   Path("qrs").mkdir(parents=True, exist_ok=True)
  # Open the workbook and select the first worksheet
   wb = xlrd.open_workbook(inputfile)
   sh = wb.sheet_by_index(0)
   # List to hold dictionaries
   data_list = []
   names_list = []
   # Iterate through first row in worksheet and fetch values into list
   for colnum in range(1, sh.ncols):
     names_list.append(unidecode.unidecode(
         (sh.cell(0, colnum).value).split(' ', 1)[0]))
   # Iterate over all the cels except first row and fecth values into dict
   for rownum in range(1, sh.nrows):
     data = OrderedDict()
     for colnum in range(1, sh.ncols):
        #  data[names_list[colnum-1]] = unidecode.unidecode(str(row_values[colnum]))
        if (colnum == 5):
          # La columna de numeros telefonicos evitamos explicit cast
          data[names_list[colnum-1]] = sh.cell_value(rownum, colnum)
        else:
         data[names_list[colnum-1]
              ] = unidecode.unidecode(str(sh.cell_value(rownum, colnum)))
     data_list.append(data)

    # QR Generation - gracias a qrcode
   for data in data_list:
       j = json.dumps(data, sort_keys=True, ensure_ascii=False)
       qr = qrcode.QRCode(
           version=1,
           error_correction=qrcode.constants.ERROR_CORRECT_H,
           box_size=5,
           border=3,
       )
       qr.add_data(j)
       qr.make(fit=True)
       img = qr.make_image(fill_color="black", back_color="white")
       nombre = data['Nombre'].replace('"', '')
       img.save('qrs/'+nombre+'.png')


if __name__ == "__main__":
  ayuda = 'Se requiere el siguiente formato: \n'
  ayuda += 'xlsx2qr.py <archivoExcel> \n'
  ayuda += 'La salida puede encontrarse en la carpeta qrs \n'

  parser = argparse.ArgumentParser(add_help=False)
  parser._positionals.title = 'Argumentos posicionales'
  parser._optionals.title = 'Argumentos opcionales'
  parser.add_argument('archivo', help="archivo Excel a procesar")
  parser.add_argument('-v', dest='verbose',
                      help="Ver informacion adicional", action='store_true')
  parser.add_argument('-h', '--help', action='help', default=argparse.SUPPRESS,
                      help=ayuda)
  args = parser.parse_args()
  main(args)
