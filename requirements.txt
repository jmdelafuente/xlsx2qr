argparse>=1.4.0
pathlib>=1.0.1
qrcode>=6.1
simplejson>=3.17.0
xlrd>=1.2.0
unidecode>=1.1.1